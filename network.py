#!/usr/bin/python3

import sys
from os import path

# Checking command line args
if len(sys.argv) != 3:
    print("Usage: python3 network.py [input_file] [output_file] ")
    sys.exit(1)

INPUT_FILE=sys.argv[1]
OUTPUT_FILE=sys.argv[2]

#Checking input file exists
if (path.exists(INPUT_FILE)==False) or (path.isdir(INPUT_FILE)):
    print("Input file not found")

#Main cycle
input_file = open(INPUT_FILE)
output_file= open(OUTPUT_FILE,"w")
for input_line in input_file.readlines():
    if '/' in input_line:
        output_line=input_line.strip()+ ' reject;'
    else:
        output_line = input_line.strip() + '/32 reject;'
    output_line='route '+output_line+'\n'
    output_file.write(output_line)
input_file.close()
output_file.close()
